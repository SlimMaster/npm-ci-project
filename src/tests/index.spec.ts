import testDomain from "../domain";

describe("local test", () => {
  it("should give correct array", () => {
    expect(testDomain.testFunction(["aa", "bb", "abcde"])).toEqual([
      "abcde_modified",
    ]);
  });
});
