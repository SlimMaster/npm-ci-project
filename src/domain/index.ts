const testFunction = (listArray: string[]) => {
  return listArray.filter((e) => e.length >= 4).map((e) => `${e}_modified`);
};

export default {
  testFunction,
};
