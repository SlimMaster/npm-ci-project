/// <reference types="vitest" />
import path from "path";
import { defineConfig } from "vite";
import baseConfig from "./vite.config.base";

export default defineConfig(() => ({
  ...baseConfig,
  build: {
    lib: {
      entry: path.resolve(__dirname, "src/main.ts"),
      name: "MyPackageTest",
      fileName: "my-package-test",
    },
  },
}));
