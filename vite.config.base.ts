/// <reference types="vitest" />
import { fileURLToPath, URL } from "url";

import vue from "@vitejs/plugin-vue";

export default {
  base: "",
  publicDir: "./static",
  test: {
    globals: true,
    environment: "jsdom",
    silent: false,
    coverage: {
      provider: "istanbul",
      reporter: ["text", "html"],
      include: ["src/*/.vue", "src/composables/*/.ts"],
    },
  },
  plugins: [vue()],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  define: {
    _VUE_PROD_HYDRATION_MISMATCH_DETAILS_: false,
  },
};
