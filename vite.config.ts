/// <reference types="vitest" />
import { defineConfig } from "vite";

import baseConfig from "./vite.config.base";

export default defineConfig({
  ...baseConfig,
  server: {
    port: 4000,
  },
});
