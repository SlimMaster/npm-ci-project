/* eslint-env node */
require("@rushstack/eslint-patch/modern-module-resolution");

module.exports = {
  root: true,
  extends: [
    "plugin:vue/vue3-essential",
    "eslint:recommended",
    "@vue/eslint-config-typescript/recommended",
    "@vue/eslint-config-prettier",
  ],
  rules: {
    "vue/multi-word-component-names": [
      "error",
      {
        ignores: [
          "Attachment",
          "Attachments",
          "SearchBar",
          "Bubble",
          "Modal",
          "Button",
          "Toast",
          "Toggle",
          "Home",
          "Settings",
          "Divider",
          "Message",
          "Results",
          "Timer",
        ],
      },
    ],
    "no-unused-vars": [
      "error",
      {
        vars: "all",
        args: "after-used",
        caughtErrors: "all",
      },
    ],
    quotes: ["warn", "single", { allowTemplateLiterals: true }],
  },
  overrides: [
    {
      files: [".d.ts", "./types/*"],
      rules: {
        "no-unused-vars": "off",
      },
    },
  ],
  env: {
    node: true,
    "vue/setup-compiler-macros": true,
  },
};
