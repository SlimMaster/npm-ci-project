#!/bin/bash

. "$(dirname "$0")/config.sh"

echo "Detected git branch ${BRANCH_NAME} in $(dirname "$0")"

NPM_TAG=latest

if [ "${BRANCH_NAME}" = "main" ]; then
  echo "Detected git branch ${BRANCH_NAME}"
  echo "Detected package version ${PACKAGE_VERSION}"
elif [ "${BRANCH_NAME}" = "dev" ]; then
  echo "Detected git branch ${BRANCH_NAME}"
  VERSION="${PACKAGE_VERSION}-dev"
  echo "Set package version to ${VERSION} ..."
elif [ ${#JIRA_ISSUE} -ge 1 ]; then
  echo "Detected git branch ${BRANCH_NAME} with JIRA issue ${JIRA_ISSUE}"
  VERSION="${PACKAGE_VERSION}-${JIRA_ISSUE}"
  echo "Set package version to ${VERSION} ..."
  NPM_TAG=${JIRA_ISSUE}
  npm version ${VERSION} || exit 1
else
  echo "Skip publishing because git branch ${BRANCH_NAME} or ${JIRA_ISSUE} is unsupported"
  exit 0
fi

echo "Publish package to npm registry ${NPM_REGISTRY} ..."
npm publish --registry ${NPM_REGISTRY} --tag ${NPM_TAG} --verbose|| exit 1

# if [ "${BRANCH_NAME}" = "master" ]; then
#   GIT_TAG="v${PACKAGE_VERSION}"
#   echo "Set git tag ${GIT_TAG} ..."
#   git tag -d ${GIT_TAG}
#   git push origin :refs/tags/${GIT_TAG}
#   git tag ${GIT_TAG} || exit 1
# fi

# echo "Push git tag ${GIT_TAG} to ${GIT_URL} ..."
# git remote add central ${GIT_URL} || exit 1
# git push central --tags || exit 1