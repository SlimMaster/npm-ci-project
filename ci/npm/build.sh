#!/bin/bash

. "$(dirname $0)/config.sh"

set -e

echo "Start building ${PROJECT_NAME}"

if [ "${BRANCH_NAME}" = "main" ]; then
  echo "RUN 'npm run build-lib' for production 'npm run build-lib' ..."
  npm run build-lib || exit 1
elif [ "${BRANCH_NAME}" = "dev" ]; then
  echo "RUN 'npm run build-lib' for developement 'npm run build-lib' ..."
  npm run build-lib || exit 1
elif [ ${#JIRA_ISSUE} -ge 1 ]; then
  echo "RUN 'npm run build-lib' for developement with branch tag ${BRANCH_NAME}' ..."
  npm run build-lib || exit 1
else
  echo "Skip build because git branch ${BRANCH_NAME} or Jira issue ${JIRA_ISSUE} is unsupported"
  exit 0
fi

