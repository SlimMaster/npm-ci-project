#!/bin/bash

export BRANCH_NAME=$(echo $CI_COMMIT_REF_NAME | tr '[:upper:]' '[:lower:]' | sed 's/[\.\/]/\-/g')
export JIRA_ISSUE=$(echo $BRANCH_NAME | grep -E -o '((SLIM|slim)-[0-9]+)')
export NPM_REGISTRY="https://registry.npmjs.org/"
export PACKAGE_VERSION=$(cat package.json | grep version | head -1 | awk -F: '{ print $2 }' | sed 's/[",]//g' | sed 's/\s//g')
export PROJECT_NAME=$(cat package.json | grep name | head -1 | awk -F: '{ print $2 }' | sed 's/[",]//g' | sed 's/\s//g')

echo "------ unknown -------"
echo "CI_PIPELINE_SOURCE ${CI_PIPELINE_SOURCE} ..."







echo "------ resolved -------"
echo "NPM_REGISTRY ${NPM_REGISTRY} ..."
echo "PACKAGE_VERSION ${PACKAGE_VERSION} ..."
echo "CI_COMMIT_BRANCH ${CI_COMMIT_BRANCH} ..."
echo "CI_COMMIT_TAG ${CI_COMMIT_TAG} ..."
echo "CI_COMMIT_TAG_MESSAGE ${CI_COMMIT_TAG_MESSAGE} ..."
echo "CI_OPEN_MERGE_REQUESTS ${CI_OPEN_MERGE_REQUESTS} ..."
echo "CI_MERGE_REQUEST_SOURCE_BRANCH_NAME ${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME} ..."
echo "CI_MERGE_REQUEST_TARGET_BRANCH_NAME ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME} ..."
echo "CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_NAME ${CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_NAME} ..."

echo "CI_COMMIT_REF_NAME ${CI_COMMIT_REF_NAME} ..."
echo "CI_PIPELINE_SOURCE ${CI_PIPELINE_SOURCE} ..."
echo "target chat ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME} ..."
echo "source ${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}  ..."